/*
 * uart1_conf.h
 *
 * Created: 18/09/2017 06:55:43 p.m.
 *  Author: hydro
 */ 


#ifndef UART1_CONF_H_
#define UART1_CONF_H_

#include "sam.h"

void uart1_config();
void transmitByte(uint8_t data);
void printString(const char myString[]);
void printNewLine();
void UART1_Handler();



#endif /* UART1_CONF_H_ */