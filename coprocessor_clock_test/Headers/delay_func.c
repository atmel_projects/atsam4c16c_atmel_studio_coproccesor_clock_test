/*
 * delay_func.c
 *
 * Created: 18/09/2017 07:06:12 p.m.
 *  Author: hydro
 */ 

#include "delay_func.h"

//This is a blocking loop - it's not very good to use.  There is an example of a timer based delay timer/counter2 example.
void delay (void){
	for (volatile uint16_t x=0; x<65535;x++){
		asm ("nop");
	}
}
